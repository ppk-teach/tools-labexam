# Lab exam for CS251

**April 18 2015, Saturday**.

## Instructions.

We will essentially follow the same instructions that we follow in
an assignment submissions

1. Create a private repository `tools-labexam` for your lab-exam and
   share it with your TA before hand. This should be an empty
   repository with no contents.

2. Create a local copy of this skeleton repository. This file will
   have the questions on exam day.

        git clone git@bitbucket.org:ppk-teach/tools-labexam.git
        cd tools-labexam    # move into the repository

        git remote add upstream git@bitbucket.org:your-user-name/tools-labexam.git
		                    # you will be subsequently pushing to your main repository.
		git push -u upstream master.
		hack push hack push etc
		show demo to TA.

3. You will need to do a demo of you assignment to your TA during lab
   duration.

4. The total marks for this lab exam is 50.

5. You can try out the work flow before the exam so that you are
   confident about it.

6. The exam will be held in two different locations and I will be
   awailable only on one of the locations. However, I (@piyush-kurur)
   will be on irc chat `##cseiitk-programming-tools @ Freenode` . So
   any queries can be asked there. Also use the issue tracker.

## Questions.

## Computing the value of Pi.

**Maximum marks = 30**

Estimate the value of π using the following random
experiment. Consider the unit circle C centred at the origin. Enclose
this circle in a square with end points (±1,±1). Consider the uniform
distribution of points in the square. Estimate the probability by
which it is inside the circle C. The value of π can be estimated from
this.

The experiment is to be done in octave. A sample for us will be a set
of 1000 points inside the square generated uniformly at random. For
such a sample, computing the number of points in the circle will give
an estimate of probability of the event that a randomly choosen point
is inside the circle C and this will lead to an estimate of π.  Now
generate 10³ such samples sets of 1000 points each and in each compute
the number of points inside the circle.

1. Write octave code to perform the above experiment. Your source
   repository should contain the source code and the generated
   points.

   * Create a single file called `samples` that contains all the 1000
	 sample sets. i.e. it will contain 1000 * 1000 points from the
	 square.

   * Create a single file called `frequencies` that contain the
	 frequencies of success. This will contain 1000 entries. The ith
	 entry will have the number of times a point inside the circle was
	 sampled in the ith sample set of 1000 points. Use this to plot
	 the histogram.

2. You also need generate a latex report with the following.

   * A write up explaining the experiment with a tikz picture showing
	 the square and circle figure. Shade the inside of the circle with
	 a different colour to show the region of success.
   * A histogram using gnuplot for the 10³ samples. Choose the width
	 of of the cells of the histogram appropriately. You can use
	 either gnuplot, octave or tikz to draw the histograms.
   * For the histogram you need to bin the 1000 sample sets
	 appropriately so that it approximates the probability
	 distribution function of the binomial distribution. Use the peak
	 to estimate the value of π.

	 NOTE: In the histogram you can plot the actual frequencies or the
	 values of pi calculated from the frequencies. Whichever it is the
	 latex document should clarify it.

## Computing the graph of a git repository.

**Maximum marks: 20**

A git repository consists of a series of commits each having an hash
id.  You can see those id by typing the git log command (see the lines
of the form `commit: 5c6bfa047233c32c48dc29a5fce1691e760c7e89` below).

```bash

$ git log

commit 5c6bfa047233c32c48dc29a5fce1691e760c7e89
Merge: 8dc51f0 1f419d4
Author: Piyush P Kurur <ppk@cse.iitk.ac.in>
Date:   Fri Apr 10 10:29:55 2015 +0530

    Merge missing marks from Dhruv

commit 8dc51f0a067ba9aef94c98ba92e6d0bb09c288e8
Merge: ae5c58a 0419cba
Author: Piyush Kurur <ppk@cse.iitk.ac.in>
Date:   Fri Apr 10 10:24:05 2015 +0550

    Merged in Grading policy for assignment 5

...

```

Each commit baring the first commit has at least one or, in case it is
a merge, more parents. The task is to write a program that prints the
commit graph of the repository. The program if run inside a git
repository should print on the standard output, the commit graph of
the repository in graphviz notation. The commit graph is a DAG
(directed acyclic graph) where there is a directed edge for a parent
to all its children.

Hint: While `git log` prints a lot of stuff, it has options to print
few selected information in appropriate format. You can use this
together with some shell and awk scripting to get the task done. For
information check out `git log --help`.

Here are the requirements

* In the graph the node labels should be abbreviated commit id.

* The command should be named gitgraph.sh and running it should produce
  the commit graph in a format suitable to be passed to the `dot` command.
  For example the following command

        ./gitgraph.sh | dot -Tps > out.ps

  should generate the ps file that contains the commit graph.

* For illustration purposes check out this repository <https://bitbucket.org/ppk-teach/sample-repository/commits/all>
  The expected output of the command given above will be some thing like (modulo slightly different layout generated by dot)
  in the ps file `sample-repo.ps`.
